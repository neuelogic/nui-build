import fs from 'fs-extra';
import {EOL} from 'os';
import path from 'path';
import {ResourceRef} from 'nui-utils';

const MAP_FILE_EXT = "js";

export default class NeueResourceMapGenerator {
	constructor(compiler) {
		compiler.Logger.attach(this, 'NeueResourceMapGenerator');
		this.log('Initializing');

		this.compiler = compiler;
		this.dispatcher = this.compiler.dispatcher;
		this.BuildOutputObject = this.compiler.BuildOutputObject;
		this.Meta = this.compiler.BuildOutputObject.Meta;

		this.Maps = {};

		this.fixes = (ref, path, depObj, res) => `
			NeueUI.register('${ref}', {
				factory: require('${path}').default,
				dependencies: ${JSON.stringify(depObj || {})}
			});
		`;

		this.sourceStart = `'use strict';

			module.exports = function(NeueUI){
		`.split(EOL).map( (l) => l.trim() ).join(EOL);

		this.sourceEnd = '}';

		this.log("Beginning Resource Map Generator");

		fs.mkdirp(this.compiler.Config.ResourceMapsFolder, (e) => {
			if(e) return this.dispatcher.reject("NeueResourceMapGenerator", e);
			this.generateIncludors();
		});
	}

	generateIncludors() {
		const Config = this.compiler.Config;
		const depTree = this.Meta.dependencyTree;
		const resources = this.Meta.resourceList;

		this._resDone = [];
		this._maps = Object.keys(depTree).map( (ref) => {
			let refObj = new ResourceRef(ref);
			let refs = this.getKeysDeep(depTree[ref]);

			let filename = path.join(
				Config.ResourceMapsFolder,
				refObj.serialize() + "." + MAP_FILE_EXT
			);
			let source = this.sourceStart;

			this.log(`Generating resource map for: ${ref}`, this.LOG_LOUD);

			let imports = refs.concat([ref]).map( (resRef) => {
				let res = resources[resRef];
				let theseDeps = { ...res.dependencies };
				Object.keys(theseDeps).forEach(
					k => theseDeps[k] = theseDeps[k].map(v => String(v))
				);

				source += this.fixes(
					resRef,
					path.relative(
						Config.ResourceMapsFolder,
						res.path
					),
					theseDeps
				);

				return res;
			});

			source += this.sourceEnd;

			this.log(`Creating file for: ${ref} -- ${filename}`, this.LOG_DEBUG);

			fs.writeFile(filename, source, (e) => {
				if(e) return this.dispatcher.reject("NeueResourceMapGenerator", e), void 0;
				this.handleSuccessfulWrite(filename,refObj);
			});

			return filename;
		});

		this.processComplete();
	}

	getKeysDeep(obj) {
		let keys = [];
		for(let x in obj) {
			if(typeof obj[x] === "object") {
				keys = keys.concat(
					this.getKeysDeep(obj[x])
				)
			}

			keys.push(x);
		}

		return keys;
	}

	handleSuccessfulWrite(filename, ref) {
		this.log(`Write successful for: ${filename}`, this.LOG_LOUD);
		this.Maps[ref.serialize()] = filename;
		this._resDone.push(filename);
		this.processComplete();
	}

	processComplete() {
		const Compiler = this.compiler;

		if(!Compiler._compareArrs(
			this._resDone, this._maps
		)) return;

		this.log(`Finished Generating Resource Maps (${this._maps.length} created)`);

		this.dispatcher.resolve('NeueResourceMapGenerator', this);
	}
}
