import fs from 'fs-extra';

const rmRf = (path, Compiler) => {
	if( fs.existsSync(path) ) {
		fs.readdirSync(path).forEach( (file,index) => {
			var curPath = path + '/' + file;
			if(fs.lstatSync(curPath).isDirectory()) { // recurse
				rmRf(curPath, Compiler);
			} else { // delete file
				fs.unlinkSync(curPath);
			}
		});
		Compiler.log(['NeueClean', `Removing directory: ${path}`], Compiler.LOG_DEBUG);
		fs.rmdirSync(path);
	}
}

export default (Compiler) => {
	const Config = Compiler.Config;

	Compiler.log(['NeueClean', 'Cleaning up everyone else\'s messes...']);

	try {
		for (let fldr of Config.CleanPaths) rmRf(fldr, Compiler);
		if (Config.NodeSiteModulesPath.indexOf('node_modules') < 0) {
			rmRf(Config.NodeSiteModulesPath, Compiler);
		}
	} catch(e) {
		Compiler.dispatcher.reject('NeueClean', e);
		return;
	}

	Compiler.dispatcher.resolve('NeueClean', true);
}
