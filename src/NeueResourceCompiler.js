var fs = require('fs');
var path = require('path');
var gutil = require("gulp-util");

var ResourceRef = require('nui-utils').ResourceRef;
var ValueTypeFilters = require('nui-utils').ValueTypeFilters;

function LogBuildMessage(msg, verboseOnly){
	if(verboseOnly && !BUILD_ARGS.hasOwnProperty('verbose')) return;
	gutil.log("[webpack] [NeueLoader]", msg);
};

module.exports = function(source) {
	var file = this.request.split(/\!/).pop().split(/\?/)[0];
	var existsWithinModule = null;
	var resourceType = null;
	var resourceMeta = null;

	var booMods = NEUE_BUILD_OUTPUT_OBJECT.SiteModules;
	for(var mod of booMods) {
		if(existsWithin(mod.path, file)) {
			existsWithinModule = mod;

			for(var type of ['Actions', 'Pages', 'Views']) {
				if(existsWithin(
					path.join( mod.path, type.toLowerCase() ) + "/", file
				)) {
					resourceType = type;
					var resources = existsWithinModule.Types[type];
					for(var res of resources) {
						if(ValueTypeFilters.String(res)) continue;

						var resRef = res.ref;
						var fileLoc = path.join( res.path, res.name);

						if(fileLoc === file) {
							resourceMeta = res;
							break;
						}
					}

					break;
				}
			}

			break;
		}
	}

	/**
	 * @TODO: Will cachability conflict with our efforts to dissolve
	 * resources when they're not longer needed for the page??
	 */
	if(existsWithinModule && resourceType) {
		LogBuildMessage("Loading NeueUI "+resourceType+" Resource: `"+file+"`", true);
		LogBuildMessage("Module exists within `"+existsWithin.ref+"` module", true);
		this.cacheable();
	}

	if(resourceMeta) {
		LogBuildMessage("Resource has meta. Retrieving dependencies list.", true);
		for(var type of ['actions', 'pages', 'views']) {
			if(!resourceMeta.hasOwnProperty(type)) continue;

			LogBuildMessage("Retrieving "+type+" dependencies.", true);
			for(var ref of resourceMeta[type]) {
				var modPath, fileName;
				if(ref.indexOf(path.sep) === -1) {
					modPath = existsWithinModule.path;
					fileName = ref + ".js";
				} else {
					var parts = ResourceRef(NEUE_BUILD_OUTPUT_OBJECT, ref);
					modPath = NEUE_BUILD_OUTPUT_OBJECT.SiteModules[parts[0]].path;
					fileName = parts[0] + ".js";
				}

				var referencedDependency = path.join(modPath, type, fileName);

				LogBuildMessage("Including resource as dependency: `"+referencedDependency+"`", true);

				this.addDependency(referencedDependency);
				source = "require('"+referencedDependency+"');" + source;
			}
		}
	}

	return source;
}

function existsWithin( modPath, file ) {
	return (path.relative(modPath, file).indexOf('..') === -1)
}
