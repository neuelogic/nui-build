#!/usr/bin/env node

import path from 'path';
import util from 'util';
import argv from 'yargs';
import gutil from 'gulp-util';

import CompilerFactory from './index';
import BabelBuilder from 'nui-builder-babel';
import BuildWatchman from 'nui-build-watch';

import * as WorkerDomain from 'domain';

const CWD = process.cwd();
const SRC_REL_PATH = './src/';
const SRC_PATH = path.join(CWD, SRC_REL_PATH);

const BUILD_ARGS = global.BUILD_ARGS = argv
	.alias('c', 'clean')
	.boolean('c')

	.alias('w', 'watch')
	.boolean('c')

	.alias('t', 'target')
	.default('t', 'all')

	.alias('l', 'loglevel')
	.choices('l', ['quiet', 'loud', 'debug'])
	.default('l', 'quiet')

	.alias('C', 'config')

	.alias('D', 'dump')
	.boolean('D')
	.default('D', false)
.argv;

const executor = () => {
	const Compiler = CompilerFactory({
		Target: BUILD_ARGS['t'],
		BuildContext: path.resolve(CWD),
		DefaultModulePath: function() {
			return path.join(this.TempOutputFolder, SRC_REL_PATH);
		}
	});

	Compiler.dispatcher.when('NeueClean').then( () => new BabelBuilder(
		SRC_PATH,
		Compiler.Config.DefaultModulePath,
		Compiler
	)).catch(e => Compiler.compileError(e));

	Compiler.init(
		BUILD_ARGS.clean,
		BUILD_ARGS.dump
	);

	return Compiler;
};

if(BUILD_ARGS['w']) {
	const Watchman = new BuildWatchman(CWD, SRC_PATH, executor);
} else executor();
