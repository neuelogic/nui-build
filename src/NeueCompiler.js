'use strict';

global.BUILD_ARGS = global.BUILD_ARGS || {};

import os from 'os';
import fs from 'fs-extra';
import path from 'path';
import util from 'util';
import NeueLogger from 'nui-logger';
import NeueBuildConfig from './NeueBuildConfig.js';
import { Dispatcher } from 'thenable-events';
import { ValueTypeFilters } from 'nui-utils';
import NeueAnalyzer from './analyzers/NeueAnalyzer.js';
import NeueBuilder from './builders/NeueBuilder.js';
import NeueWebpack from './NeueWebpack.js';
import NeueResourceMapGenerator from './NeueResourceMapGenerator.js';
import * as WorkerDomain from 'domain';

import webpack from 'webpack';

const defineImmutableProp = (name, val, obj = this) => {
	Object.defineProperty(obj, name, {
		configurable: false,
		writable: false,
		enumerable: false,
		value: val
	});
}

/**

	1. Kick Off SiteAnalyzer, listen for completion
	2. Pass result to new Builder, listen for completion
	3. Iterate through result, pass to webpack

**/

export default class NeueCompiler {
	constructor(buildConfig) {
		this.initLogger();
		this.log('Initializing');

		this.initDispatcher();
		this.initConfig(buildConfig);
	}

	// Init Logger
	initLogger() {
		const Logger = new NeueLogger({
			namespace: 'NeueCompiler',
			verbosity: NeueLogger[`LOG_${BUILD_ARGS.loglevel.toUpperCase()}`]
		});
		this.use(Logger.attach.bind(Logger));
		defineImmutableProp('Logger', Logger, this);
	}

	// Init Dispatcher
	initDispatcher() {
		// SingletonPromise.alwaysForce = true;
		// SingletonPromise.defaultCatch = this.compileError.bind(this);
		// this.dispatcher = new Dispatcher(SingletonPromise, this.Logger);
		defineImmutableProp('dispatcher', new Dispatcher(void 0, this.Logger), this);
	}

	initConfig(buildConfig) {
		// Init Config
		this.log('Applying Configuration', this.LOG_LOUD);
		this.Config = new NeueBuildConfig( Object.assign({
			'BuildContext': path.dirname( module.parent.filename ),
			'BuildArgs': BUILD_ARGS
		}, (buildConfig || {}) ) );

		// Init Build Config
		this.BuildOutputObject = {
			'project': {
				'dir': this.Config.BuildContext
			}
		};
	}

	use(middleware) {
		if(ValueTypeFilters.HasPrototype(middleware)) {
			return new middleware(this);
		} else {
			return middleware(this);
		}
	}

	copyModules() {
		WorkerDomain.create().on('error', this.compileError.bind(this)).run( () => {
			var nuiModules = this.Config.NodeSiteModulesPath,
				nodeModules = path.join(this.Config.BuildContext, 'node_modules');

			fs.mkdirpSync(nuiModules);

			this.getFolders(nodeModules).filter(
				(d) => d.substr(0,8)==='nui-pkg-' // Only include 'nui-' packages
			).map( (d) => {
				fs.copySync(
					path.join(nodeModules, d),
					path.join(nuiModules, d),
					{ 'preserveTimestamps': true }
				);

				this.dispatcher.resolve('NeueFolderCopied', [
					path.join(nodeModules, d),
					path.join(nuiModules, d)
				]);
			});

			this.dispatcher.resolve('NeueModulesCopied', true);
		});
	}

	compileError(e) {
		if(e instanceof Error) {
			this.log(['ERROR', e.stack]);
		} else {
			this.log(['ERROR', e]);
		}

		this.log(util.inspect(this.toString(), false, null), this.LOG_DEBUG);

		process.exit(1);
	}

	getFolders( dir ) {
		this.log(`Retrieving directory listing for: ${dir}`, this.LOG_DEBUG);
		return fs.readdirSync(dir).filter( (file) => fs.statSync(path.join(dir, file)).isDirectory() );
	}

	_compareArrs(arr1, arr2) {
		arr1 = (arr1||[]);
		arr2 = (arr2||[]);

		arr1.sort();
		arr2.sort();

		return (arr1.length == arr2.length) && arr1.every(function(element, index) {
			return element === arr2[index];
		});
	}

	log() { return; }

	toString() {
		function deepCopy(obj) {
			let ret = {};
			let keys = Object.keys(obj);

			keys = keys.filter( (v) => ['compiler','dispatcher','analyzer'].indexOf(v) === -1)

			for(let v of keys) {
				if(obj[v] instanceof Object) ret[v] = deepCopy(obj[v]);
				else ret[v] = obj[v];
			}

			return ret;
		}

		return JSON.stringify(deepCopy(this));
	}
}
