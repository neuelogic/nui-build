/*
	Create entryPoint configuration for resources
*/
export default class NeueResourceBuilder {
	constructor(compiler, resource) {
		compiler.Logger.attach(this, 'NeueResourceBuilder');

		this.resource = resource;
		this.compiler = compiler;
		this.dispatcher = this.compiler.dispatcher;

		this.buildEntryPointObj();
	}

	buildEntryPointObj() {
		this.log(`Configuring entry point for ${this.resource.ref}`, this.LOG_LOUD);

		var DispatcherPromise = this.dispatcher.getResolver("NeueResourceBuilder."+this.resource.ref.module);

		let res = this.resource;

		let EntryPoints = this.EntryPoints = [];

		let ResourceList = this.compiler.BuildOutputObject.Meta.resourceList;
		let DependencyTree = this.compiler.BuildOutputObject.Meta.dependencyTree;

		let DepTree = [res.ref].concat(
			this.getKeysDeep(DependencyTree[res.ref])
		);

		for(let i=0;i<DepTree.length;i++) {
			let resource = ResourceList[DepTree[i]];
			if(!resource) {
				DispatcherPromise.reject(new Error(
					"["+res.ref+"] Missing dependency: "+DepTree[i]
				));
				return;
			}

			EntryPoints.push(resource.path);
		}

		DispatcherPromise.resolve(this);
	}

	getKeysDeep(obj) {
		let keys = [];
		for(let x in obj) {
			if(typeof obj[x] === "object") {
				keys = keys.concat(
					this.getKeysDeep(obj[x])
				)
			}

			keys.push(x);
		}

		return keys;
	}
}
